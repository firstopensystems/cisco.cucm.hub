package ru.fos.cisco.cucm;

import com.cisco.cti.util.Condition;
import microsoft.aspnet.signalr.client.Action;
import microsoft.aspnet.signalr.client.ErrorCallback;
import microsoft.aspnet.signalr.client.LogLevel;
import microsoft.aspnet.signalr.client.Logger;
import microsoft.aspnet.signalr.client.hubs.HubConnection;
import microsoft.aspnet.signalr.client.hubs.HubProxy;
import microsoft.aspnet.signalr.client.transport.AutomaticTransport;

import javax.telephony.*;
import javax.telephony.callcontrol.CallControlCall;
import javax.telephony.events.*;
import java.util.HashMap;
import java.util.HashSet;

public class CallAppManager {

    String xmlUser = "";       // XML user name
    String xmlPw = "";         // XML user password
    String cmeIp = "";         // CME IP address
    Provider provider;
    HashMap<String, Address> connections = new HashMap<>();
    HashSet<String> ActiveCalls = new HashSet<>();

    private final HubConnection connection;
    private HubProxy mainHubProxy;

    public CallAppManager(String host, String Ip, String User, String Pw) throws Exception {
        cmeIp = Ip;
        xmlUser = User;
        xmlPw = Pw;

        connection = new HubConnection(host);
        mainHubProxy = connection.createHubProxy("CiscoHub");
        mainHubProxy.subscribe(this);
        System.out.println("connecting to hub");
        connection.closed(new Runnable() {
            @Override
            public void run() {
                System.out.println("Closed");
                connection.start(new AutomaticTransport(new Logger() {
                    @Override
                    public void log(String s, LogLevel logLevel) {
                        System.out.println(String.format("[%s] %s", logLevel.name(), s));
                    }
                })).done(new Action<Void>() {
                    @Override
                    public void run(Void obj) throws Exception {
                        System.out.println("connected to hub");
                    }
                });
            }
        });
        connection.error(new ErrorCallback() {
            @Override
            public void onError(Throwable throwable) {
                System.out.println("Error");
                connection.start(new AutomaticTransport(new Logger() {
                    @Override
                    public void log(String s, LogLevel logLevel) {
                        System.out.println(String.format("[%s] %s", logLevel.name(), s));
                    }
                })).done(new Action<Void>() {
                    @Override
                    public void run(Void obj) throws Exception {
                        System.out.println("connected to hub");
                    }
                });
            }
        });
        connection.start(new AutomaticTransport(new Logger() {
            @Override
            public void log(String s, LogLevel logLevel) {
                System.out.println(String.format("[%s] %s", logLevel.name(), s));
            }
        })).done(new Action<Void>() {
            @Override
            public void run(Void obj) throws Exception {
                System.out.println("connected to hub");
                connectCME();
            }
        });
    }

    public void Close() {
        connection.disconnect();
        connection.stop();
        provider.shutdown();
    }

    // connect to CME
    private void connectCME() {
        System.out.println("connecting to cisco");
        try {
            /* start up JTAPI */
            JtapiPeer peer = JtapiPeerFactory.getJtapiPeer(null);

            /* connect to the provider */
            String providerString = cmeIp;
            providerString += ";login=" + xmlUser;
            providerString += ";passwd=" + xmlPw;
            provider = peer.getProvider(providerString);

            /* wait for it to come into service */
            final Condition inService = new Condition();
            provider.addObserver(new ProviderObserver() {
                @Override
                public void providerChangedEvent(ProvEv[] eventList) {
                    if (eventList == null) {
                        return;
                    }
                    for (int i = 0; i < eventList.length; ++i) {
                        if (eventList[i] instanceof ProvInServiceEv) {
                            inService.set();
                        }
                    }
                }
            });
            inService.waitTrue();
            System.out.println("connected to cisco");
            Address[] addrs = provider.getAddresses();
            for (Address addr : addrs) {
                try {
                    addPhone(addr.getName());
                } catch (Exception e) {
                    System.out.println("can't add phone " + addr);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    } //end of connectCME

    public void addPhone(final String phone_number) throws Exception {
        System.out.println("addPhone " + phone_number);
        if (!connections.containsKey(phone_number)) {
            /* get an object for the calling terminal */
            Address srcAddr = provider.getAddress(phone_number);
            srcAddr.addCallObserver(new CallObserver() {
                String phone = phone_number;

                @Override
                public void callChangedEvent(CallEv[] eventList) {
                    if (eventList == null) {
                        return;
                    }
                    for (int i = 0; i < eventList.length; ++i) {
                        if (eventList[i] instanceof TermConnActiveEv) {
                            // I need A and B number
                            CallEv event = eventList[i];
                            Call call = event.getCall();
                            if (call instanceof CallControlCall) {
                                Address myCallingAddress
                                        = ((CallControlCall) call).getCallingAddress();
                                String A_number = myCallingAddress.getName();
                                Address myCalledAddress
                                        = ((CallControlCall) call).getCalledAddress();
                                String B_number = myCalledAddress.getName();
                                if (!ActiveCalls.contains(phone)) {
                                    mainHubProxy.invoke("IncommingCall", phone, B_number);
                                    System.out.println("OutcommingCall (" + phone + ") " + A_number + "->" + B_number);
                                    ActiveCalls.add(phone);
                                }
                            }
                        }
                        if (eventList[i] instanceof TermConnRingingEv) {
                            // I need A and B number
                            CallEv event = eventList[i];
                            Call call = event.getCall();
                            if (call instanceof CallControlCall) {
                                Address myCallingAddress
                                        = ((CallControlCall) call).getCallingAddress();
                                String A_number = myCallingAddress.getName();
                                Address myCalledAddress
                                        = ((CallControlCall) call).getCalledAddress();
                                String B_number = myCalledAddress.getName();
                                if (!ActiveCalls.contains(phone)) {
                                    mainHubProxy.invoke("IncommingCall", A_number, phone);
                                    System.out.println("IncommingCall (" + phone + ") " + A_number + "->" + B_number);
                                    ActiveCalls.add(phone);
                                }
                            }
                        }
                        if (eventList[i] instanceof ConnConnectedEv) {
                            // I need A and B number
                            CallEv event = eventList[i];
                            Call call = event.getCall();
                            if (call instanceof CallControlCall) {
                                Address myCallingAddress
                                        = ((CallControlCall) call).getCallingAddress();
                                String A_number = myCallingAddress.getName();
                                Address myCalledAddress
                                        = ((CallControlCall) call).getCalledAddress();
                                String B_number = myCalledAddress.getName();
                                mainHubProxy.invoke("IncommingCallAnswered", A_number, phone);
                                System.out.println("IncommingCallAnswered (" + phone + ") " + A_number + "->" + B_number);
                                if (ActiveCalls.contains(phone)) {
                                    ActiveCalls.remove(phone);
                                }
                            }
                        }
                        if (eventList[i] instanceof ConnDisconnectedEv) {
                            // I need A and B number
                            CallEv event = eventList[i];
                            Call call = event.getCall();
                            if (call instanceof CallControlCall) {
                                Address myCallingAddress
                                        = ((CallControlCall) call).getCallingAddress();
                                String A_number = myCallingAddress.getName();
                                Address myCalledAddress
                                        = ((CallControlCall) call).getCalledAddress();
                                String B_number = myCalledAddress.getName();
                                mainHubProxy.invoke("IncommingCallDisconnected", phone);
                                System.out.println("IncommingCallDisconnected (" + phone + ") " + A_number + "->" + B_number);
                                if (ActiveCalls.contains(phone)) {
                                    ActiveCalls.remove(phone);
                                }
                            }
                        }
                    }
                }
            });
            connections.put(phone_number, srcAddr);
        }
        mainHubProxy.invoke("AddPhoneComplete", phone_number);
    }

    public void makeCall(String origDn, String destDn) throws Exception {
        addPhone(origDn);
        System.out.println("makeCall " + origDn + " -> " + destDn);
        if (connections.containsKey(origDn)) {
            Call call = provider.createCall();
            call.connect(connections.get(origDn).getTerminals()[0], connections.get(origDn), destDn);
        }
    }
}
