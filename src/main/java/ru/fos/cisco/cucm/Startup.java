/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.fos.cisco.cucm;

/**
 * java daemon_app <&- 2>/var/log/daemon_app_error.log &
 * http://<ipadress-of-cucm>/plugins/jtapi.jar
 *
 * @author eugene
 */
public class Startup {

    private static final Object stop = new Object();

    public static void start(String[] args) throws Exception {
        System.out.println("main start");

        Runtime.getRuntime().addShutdownHook(
                new Thread() {
                    @Override
                    public void run() {
                        synchronized (stop) {
                            stop.notifyAll();
                        }
                    }
                }
        );
        System.out.println("start");
        // connect to device
        CallAppManager cli = new CallAppManager(args[0], args[1], args[2], args[3]);

        synchronized (stop) {
            stop.wait();
        }
        cli.Close();
    }

    public static void stop(String[] args) {
        System.out.println("main stop");
        System.out.println("stop");
        synchronized (stop) {
            stop.notifyAll();
        }
    }

    public static void main(String[] args) throws Exception {
        System.out.println("main");
        if (null != args[0]) {
            switch (args[0]) {
                case "start":
                    if (args.length != 5) {
                        System.out.println("Usage: java -jar cisco.cucm.hub.jar start \"https://myhub/\" \"10.0.4.4\" admin pass");
                        return;
                    }
                    start(new String[]{args[1], args[2], args[3], args[4]});
                    break;
                case "stop":
                    stop(args);
                    break;
            }
        }
    }
}
