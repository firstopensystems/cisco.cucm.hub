@ECHO OFF

set SERVICE_NAME=CiscoListener
set PR_INSTALL=C:\CiscoListener\prunsrv.exe
 
REM Service log configuration
set PR_LOGPREFIX=%SERVICE_NAME%
set PR_LOGPATH=c:\CiscoListener\logs
set PR_STDOUTPUT=c:\CiscoListener\logs\stdout.txt
set PR_STDERROR=c:\CiscoListener\logs\stderr.txt
set PR_LOGLEVEL=Error
 
REM Path to java installation
set PR_JVM=%JRE_HOME%\bin\client\jvm.dll
set PR_CLASSPATH=C:\CiscoListener\cisco.cucm.hub.jar
 
REM Startup configuration
set PR_STARTUP=auto
set PR_STARTMODE=jvm
set PR_STARTCLASS=ru.fos.cisco.cucm.Startup
set PR_STARTMETHOD=start
set PR_STARTPARAMS="https://myhub/";"10.0.4.4";admin;pass
 
REM Shutdown configuration
set PR_STOPMODE=jvm
set PR_STOPCLASS=ru.fos.cisco.cucm.Startup
set PR_STOPMETHOD=stop
 
REM JVM configuration
set PR_JVMMS=256
set PR_JVMMX=1024
set PR_JVMSS=4000
set PR_JVMOPTIONS=-Duser.language=RU;-Duser.region=ru
 
REM Install service
prunsrv.exe //IS//%SERVICE_NAME%
